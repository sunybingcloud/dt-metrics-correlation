```commandline
usage: correlate_factors.py [-h]
                            [--kind {video-encoding,cryptography,compilation}]
                            [--powerclass POWERCLASS [POWERCLASS ...]]
                            [--nodes-cpuarchinfo NODES_CPUARCHINFO]
                            [--benchmark BENCHMARK] [--host HOST]
                            [--pcplog-freq-seconds PCPLOG_FREQ_SECONDS]
                            [--correlation-criteria {average,median,95PCTL,90PCTL}]
                            [--plot [PLOT]]
                            [--correlation-target-metric {exectime,cpuquota}]

Correlation of various factors to performance impact

optional arguments:
  -h, --help            show this help message and exit
  --kind {video-encoding,cryptography,compilation}
                        Kind of applications to consider (required).
  --powerclass POWERCLASS [POWERCLASS ...]
                        Powerclass of hosts. If no powerclass given, then
                        correlation performed across all powerclasses
                        (required).
  --nodes-cpuarchinfo NODES_CPUARCHINFO
                        JSON file containing hostnames and their respective
                        #cpus (required).
  --benchmark BENCHMARK
                        Name of the benchmark as in the log file name
                        (required)
  --host HOST           Name of the host on which the benchmark was profiled
                        (required). NOTE: SPECIFY HOSTNAME WITHOUT
                        '.cs.binghamton.edu'.
  --pcplog-freq-seconds PCPLOG_FREQ_SECONDS
                        The frequency at which PCP logs were fetched and
                        written to the file.
  --correlation-criteria {average,median,95PCTL,90PCTL}
                        Correlation criteria.
  --plot [PLOT]         Activate plotting of metric data variations against
                        performance. The correlation criteria is used to fetch
                        the data.
  --correlation-target-metric {exectime,cpuquota}
                        Target metric to correlate with task metrics
```
