import sys
import os
import plotly.graph_objects as go
#import plotly.express as px
import plotly.io as pio
import pandas as pd
import collections

#TODO:
'''
1. Read the file into pandas data frame and plot - done
2. Get all files from the directory
'''
def printUsage():
    print('''Correct Usage: python trends_plot.py <mode> <app_file> <xaxis> <yaxis> <image_file_name> where
                mode represents type of graph, mode takes values {single, multiple, all}
                app_file represents the application for which we are plotting graph. If mode is single, app_name is expected to be the task_metrics file name, else app_name represents the directory path that contains the runs
                xaxis represents the xaxis metric {"time" to generate graphs against execution time}                     
                yaxis represents the yaxis metric {"all" to generate graphs for all columns}
                image_file_name represents the name of hte png file name
                ''')

def getTaskMetricFilesFromDirectory(app_dir):
    app_to_files_map = collections.defaultdict(list)
    for power_class in os.listdir(app_dir):
        for app_quota in os.listdir(app_dir+"/"+power_class):
            parts = app_quota.split("__")
            app_name = parts[0]
            quota = parts[1]
            filename = list(filter(lambda x: x.startswith("task_metrics"), os.listdir(app_dir+"/"+power_class+"/"+app_quota)))[0]
            app_to_files_map[app_name+"_"+power_class].append((quota, app_dir+"/"+power_class+"/"+app_quota+"/"+filename))
    return app_to_files_map
            

def generateEmptyPlot(title, xaxis_name, yaxis_name):
    fig = go.Figure()    
    fig.update_layout(title=title,
                   xaxis_title=xaxis_name,
                   yaxis_title=yaxis_name)    
    return fig

def addLineGraphToPlot(fig, x_data, y_data, name=""):
    fig.add_trace(go.Scatter(name=name, x=x_data, y=y_data,
                    mode='lines'))
    

def plotMultipleTasksRun(app_file, xaxis, yaxis, df=None):
    if(not os.path.exists(app_file)):
        raise Exception("Input file does not exist")    
    fig = generateEmptyPlot(app_file.split(".")[0], xaxis, yaxis)
    if(df is None):
        df = pd.read_csv(app_file)
    for name, dfobj in df.groupby("taskId"):
        if(xaxis == "time"):
            xdata = list(range(len(dfobj[yaxis])))
        else:
            xdata = df[xaxis]
        addLineGraphToPlot(fig, xdata, dfobj[yaxis], name=name)
    #fig.show()
    pio.write_image(fig, sys.argv[5]+"_"+xaxis+"_"+yaxis+".png", format="png")
    return

def plotAllYAxisAgainstTime(app_file, xaxis):
    df = pd.read_csv(app_file)
    for yaxis in list(df):
        plotMultipleTasksRun(app_file, xaxis, yaxis, df)

def plotSingleRun(app_file, xaxis, yaxis):
    if(not os.path.exists(app_file)):
        raise Exception("Input file does not exist")    
    fig = generateEmptyPlot(app_file.split(".")[0], xaxis, yaxis)
    df = pd.read_csv(app_file)
    if(xaxis == "time"):
        xdata = list(range(len(df[yaxis])))
    else:
        xdata = df[xaxis]
    addLineGraphToPlot(fig, xdata, df[yaxis])
    #fig.show()
    pio.write_image(fig, sys.argv[5]+".png", format="png")
    return

def plotAllRuns(app_file, xaxis, yaxis):
    if(not os.path.exists(app_file)):
        raise Exception("Input directory does not exist")
    
    category_name = app_file.split("/")[-1]
    
    #all_files is a dictionary app_name as key and all runs as a list
    all_files = getTaskMetricFilesFromDirectory(app_file)
    for app_name_power_class, metric_files in all_files.items():
        #fig header nmae format -> <category_name>_<app_name>
        fig = generateEmptyPlot(category_name + "_" + app_name_power_class, xaxis, yaxis)    
        metric_files.sort(key=lambda x: int(x[0].split("-")[1]))
        for quota, metric_file in metric_files:
            if(os.path.getsize(metric_file) == 0):
                continue
            df = pd.read_csv(metric_file)
            if(xaxis == "time"):
                xdata = list(range(len(df[yaxis])))
            else:
                xdata = df[xaxis]
            addLineGraphToPlot(fig, xdata, df[yaxis], quota)
        parts = app_name_power_class.split("_")
        dest_dir = "plots/"+parts[0]+"/"+parts[1]+"/"+parts[2]
        if(not os.path.exists(dest_dir)):
            os.makedirs(dest_dir)
        fig.write_image(dest_dir+"/"+xaxis+":"+yaxis+".png")
    return
    


if __name__ == "__main__":
    if(len(sys.argv) != 6):
        printUsage()

    #mode represents type of graph, mode takes values {single, all}
    mode = sys.argv[1]
    #app_file represents the application for which we are plotting graph. If mode is single, app_file is expected to be the task_metrics file path, else app_file represents the directory path that contains the runs
    app_file = sys.argv[2]
    #xaxis represents the xaxis metric
    xaxis = sys.argv[3]
    #yaxis represents the yaxis metric
    yaxis = sys.argv[4]


    if(mode == "single"):
        #call the inner function directly on the input file.
        try:
            plotSingleRun(app_file, xaxis, yaxis)
        except Exception as e:
            print(e)
    elif(mode == "multiple"):
        try:
            if(yaxis == "all"):
                plotAllYAxisAgainstTime(app_file, xaxis)
            else:
                plotMultipleTasksRun(app_file, xaxis, yaxis)
        except Exception as e:
            print(e)
    elif(mode == "all"):
        #call the function to fetch all input files and iterate over the files and add sub plots to the same plot
        # this is a little different, a category of applications would be given (like video_encoding), go to each power class (A/B/C), group together all runs for an app, an then create a graph for each app. 
        #try:
        plotAllRuns(app_file, xaxis, yaxis)
        #except Exception as e:
            #print(e)
    else:
        printUsage()

