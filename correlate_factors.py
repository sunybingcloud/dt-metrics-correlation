#!/usr/bin/python3

import argparse
import json
import os
import math
import pandas as pd
import numpy as np
from plotly.offline import plot
import plotly
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.io as pio

py.sign_in('mgovinda', '9yRhbLHSAloXMqIZ1UNZ')
plotly.tools.set_config_file(world_readable=False, sharing='private', auto_open=False)

nodes_cpuarchinfo={}

# Convert from string to boolean.
# cite - https://stackoverflow.com/questions/15008758/parsing-boolean-values-with-argparse.
def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

def setup_args():
	parser = argparse.ArgumentParser(description='Correlation of various factors to performance impact')
	parser.add_argument('--kind', default='', choices=['video-encoding', 'cryptography', 'compilation'], help='Kind of applications to consider (required).')
	parser.add_argument('--powerclass', nargs='+', help='Powerclass of hosts. If no powerclass given, then correlation performed across all powerclasses (required).')
	parser.add_argument('--nodes-cpuarchinfo', default='nodes.json', type=argparse.FileType('r'), help='JSON file containing hostnames and their respective #cpus (required).')
	parser.add_argument('--benchmark', default='', help='Name of the benchmark as in the log file name (required)')
	parser.add_argument('--host', default='', help='Name of the host on which the benchmark was profiled (required). NOTE: SPECIFY HOSTNAME WITHOUT \'.cs.binghamton.edu\'.')
	parser.add_argument('--pcplog-freq-seconds', default='1', help='The frequency at which PCP logs were fetched and written to the file.')
	parser.add_argument('--correlation-criteria', default='average', choices=['average', 'median', '95PCTL', '90PCTL'], help='Correlation criteria.')
	parser.add_argument('--plot', type=str2bool, nargs='?', const=True, default=False, help='Activate plotting of metric data variations against performance. The correlation criteria is used to fetch the data.')
	parser.add_argument('--correlation-target-metric', default='exectime', choices=['exectime', 'cpuquota'], help='Target metric to correlate with task metrics')
	return parser.parse_args()

def load_nodes_info(args):
	global nodes_cpuarchinfo
	nodes_cpuarchinfo = json.load(args.nodes_cpuarchinfo)

"""
data structure to store aggregate data for each benchmark and for each configuration.
structure:
	metrics_aggregate_data: [
		{
			run_config: {
				cpu_quota: <value>,
				cpu_shares: <value>,
				cpu_period: <value>,
			},
			perf_metrics: {
				exec_time_seconds: <value>,
			},
			task_metrics: {
				container_spec_cpu_shares: {
					average: <value>,
					median: <value>,
					95PCTL: <value>,
					90PCTL: <value>,
				},
				container_spec_cpu_quota: {
					average: <value>,
					median: <value>,
					95PCTL: <value>,
					90PCTL: <value>,
				},
				...
			},
		},
		...
	],
"""
benchmarkProfilingData = {'metrics_aggregate_data': []}

"""
task metrics list.
These are the metrics that were monitored for the running containers when profiling the benchmarks.
These are the metrics that Task-Ranker monitors and logs.
"""
taskMetricsList = [
	"container_spec_cpu_shares",
	"container_spec_cpu_quota",
	"container_spec_cpu_period",
	"container_fs_usage_bytes",
	"container_cpu_cfs_throttled_periods_total",
	"container_cpu_cfs_throttled_seconds_total",
	"cpu_util_percentage",
	"container_cpu_schedstat_runqueue_seconds_total",
	"container_cpu_schedstat_run_seconds_total",
	"container_cpu_schedstat_run_periods_total",
	"cycles_per_second",
	"instruction_retirement_rate",
	"cycles_per_instruction",
	"container_processes"
]

def setup(args):
	"""
	walk the 'kind/powerclass' directory.
	Extract the run information (cpu_quota, cpu_period, cpu_shares, host) from the log directory name.
	Setup the data structures to store the time series metrics data.
	
	Format of log directories = <kind>_<benchmark>__cpuquota-<value>__cpuperiod-100k__cpushares-<value>__host-<host>-logs
	video-encoding_vpxenc__cpuquota-3400000__cpuperiod-100k__cpushares-1024__host-stratos-005-logs
	"""
	global nodes_cpuarchinfo
	global benchmarkProfilingData

	dir_names_sorted_by_cpu_quota = [None] * int(nodes_cpuarchinfo[args.host]["cpus"] / 2) # cpus were changed by 2 for each profiling run.
	found = False
	for root, dirs, files in os.walk(os.path.join(args.kind, "powerclass-"+args.powerclass[0])): 
		for d in dirs:
			if (args.benchmark in d) and (args.host in d):
				found = True
				components = d.split('__')
				cpuquota = int(components[1].split('-')[1])
				dir_names_sorted_by_cpu_quota[int(cpuquota / 200000) - 1] = d # sorting by cpu quota.

	if not found:
		print("no logs for", args.benchmark, "in", os.path.join(args.kind, "powerclass-"+args.powerclass[0]), "for host=", args.host)
		return False
	
	for d in dir_names_sorted_by_cpu_quota:
		components = d.split('__')
		# run configuration. 
		# ignoring components[0] as it is kind_benchmark.
		cpuquota = int(components[1].split('-')[1])
		cpuperiod = int(components[2].split('-')[1].split('k')[0]) * 1000
		cpushares = int(components[3].split('-')[1])
		runConfig = {
			'cpu_quota': cpuquota,
			'cpu_period': cpuperiod,
			'cpu_shares': cpushares,
		}

		# performance metrics. 
		perfMetrics = {'exec_time_seconds': None}
		for root, dirs, files in os.walk(os.path.join(args.kind, "powerclass-"+args.powerclass[0], d)):
			for f in files:
				if "pcplog" in f:
					# cite: https://stackoverflow.com/questions/845058/how-to-get-line-count-of-a-large-file-cheaply-in-python
					perfMetrics['exec_time_seconds'] = sum(1 for line in open(os.path.join(args.kind, "powerclass-"+args.powerclass[0], d, f)))

		# task metrics.
		taskMetrics = {}
		for root, dirs, files in os.walk(os.path.join(args.kind, "powerclass-"+args.powerclass[0], d)):
			for f in files:
				if "task_metrics" in f:
					data = pd.read_csv(os.path.join(args.kind, "powerclass-"+args.powerclass[0], d, f))
					# aggregating metric data in different ways (avg, median ...).
					for task_metric in taskMetricsList:
						counter_metrics_switcher = {
							1: "container_cpu_cfs_throttled_periods_total",
							2: "container_cpu_cfs_throttled_seconds_total",
							3: "container_cpu_schedstat_runqueue_seconds_total",
							4: "container_cpu_schedstat_run_seconds_total",
							5: "container_cpu_schedstat_run_periods_total"
						}

						avg = None
						med = None
						ptile95 = None
						ptile90 = None

						if "guage" in counter_metrics_switcher.get(task_metric, "guage"):
							if data[task_metric].iloc[0] < 0: # ignoring first value if negative (cpuutil, CPI, IRR etc).
								avg = data[task_metric].tail(len(data.index)-1).mean()
								med = data[task_metric].tail(len(data.index)-1).median()
								ptile95 = np.percentile(data[task_metric].tail(len(data.index)-1), 95)
								ptile90 = np.percentile(data[task_metric].tail(len(data.index)-1), 90)
							else:
								avg = data[task_metric].tail(len(data.index)-1).mean()
								med = data[task_metric].tail(len(data.index)-1).median()
								ptile95 = np.percentile(data[task_metric].tail(len(data.index)-1), 95)
								ptile90 = np.percentile(data[task_metric].tail(len(data.index)-1), 90)
						else:
							# we only consider the last value as the metric is a counter.
							# here, avg, median, ptile95 and ptile90 would all be the same value.
							avg = data[task_metric].iloc[-1]
							median = data[task_metric].iloc[-1]
							ptile95 = data[task_metric].iloc[-1]
							ptile90 = data[task_metric].iloc[-1]

						taskMetrics[task_metric] = {
							"average": avg,
							"median": med,
							"95PCTL": ptile95,
							"90PCTL": ptile90,
						}


		benchmarkProfilingData['metrics_aggregate_data'].append({
			"run_config": runConfig,
			"perf_metrics": perfMetrics,
			"task_metrics": taskMetrics,
		})

	return True

"""
Persisting benchmarkProfilingData as a json for reference and future use.
"""
def persistBenchmarkProfilingData(args):
	global benchmarkProfilingData

	plot_dir = os.path.join("plots", args.kind, args.benchmark, "powerclass-"+args.powerclass[0])
	if not os.path.exists(plot_dir):
		os.makedirs(plot_dir, exist_ok=True)
	
	with open(os.path.join(plot_dir, "benchmarkProfilingData.json"), 'w') as f:
		json.dump(benchmarkProfilingData, f, sort_keys=True, indent=4)


"""
Correlation Coefficients of various factors.
Organized by metric.
"""
corrcoefs_exectime = {}
corrcoefs_cpuquota = {}
def correlate(args):
	global benchmarkProfilingData
	global taskMetricsList
	global corrcoefs_exectime
	global corrcoefs_cpuquota

	y_data = []
	if "exectime" in args.correlation_target_metric:
		for met_agg_data in benchmarkProfilingData['metrics_aggregate_data']:
			y_data.append(met_agg_data['perf_metrics']['exec_time_seconds'])
	elif "cpuquota" in args.correlation_target_metric:
		for met_agg_data in benchmarkProfilingData['metrics_aggregate_data']:
			y_data.append(met_agg_data['run_config']['cpu_quota'])


	for task_metric in taskMetricsList:
		x_metrics_data = []
		for met_agg_data in benchmarkProfilingData['metrics_aggregate_data']:
			x_metrics_data.append(met_agg_data['task_metrics'][task_metric][args.correlation_criteria])

		# we only care about correlation of X->Y.	
		if "exectime" in args.correlation_target_metric:
			corrcoefs_exectime[task_metric] = np.corrcoef(x_metrics_data, y_data)[0,1]
			if math.isnan(float(corrcoefs_exectime[task_metric])):
				corrcoefs_exectime[task_metric] = 0 # no correlation.

		elif "cpuquota" in args.correlation_target_metric:
			corrcoefs_cpuquota[task_metric] = np.corrcoef(x_metrics_data, y_data)[0,1]
			if math.isnan(float(corrcoefs_cpuquota[task_metric])):
				corrcoefs_cpuquota[task_metric] = 0 # no correlation.


"""
Persisting correlation of different metrics to the specified correlation target metric.
Organized by metric.
{
	<metric1>: <correlation to target metric>,
	<metric2>: <correlation to target metric>,
	...
}
"""
def persistCorrelationData(args):
	global benchmarkProfilingData
	global taskMetricsList
	global corrcoefs_exectime
	global corrcoefs_cpuquota

	plot_dir = os.path.join("plots", args.kind, args.benchmark, "powerclass-"+args.powerclass[0], "correlation_with_"+args.correlation_target_metric)
	if not os.path.exists(plot_dir):
		os.makedirs(plot_dir, exist_ok=True)

	if "exectime" in args.correlation_target_metric:
		with open(os.path.join(plot_dir, "metrics_exec-time_correlation_"+args.correlation_criteria+".json"), 'w') as f:
			json.dump(corrcoefs_exectime, f, sort_keys=True, indent=4)
	elif "cpuquota" in args.correlation_target_metric:
		with open(os.path.join(plot_dir, "metrics_cpuquota_correlation_"+args.correlation_criteria+".json"), 'w') as f:
			json.dump(corrcoefs_cpuquota, f, sort_keys=True, indent=4)


"""
Mapping of task metrics to their plot titles.
For instance, cpu_util_percentage => CPU Utilization (%).
"""
plotTitlesForTaskMetrics = {
	"container_spec_cpu_shares": "CPU Shares (count)",
	"container_spec_cpu_quota": "CPU Quota (microseconds allocated in 1 CPU period)",
	"container_spec_cpu_period": "CPU CFS Scheduler Period (microseconds)",
	"container_fs_usage_bytes": "Bytes Consumed by Container (bytes)",
	"container_cpu_cfs_throttled_periods_total": "Number of Throttled Periods (count)",
	"container_cpu_cfs_throttled_seconds_total": "Total Time Duration Container has been Throttled (seconds)",
	"cpu_util_percentage": "CPU Utilization (%)",
	"container_cpu_schedstat_runqueue_seconds_total": "Time Duration Processes of a Container have been Waiting on a Run Queue (seconds)",
	"container_cpu_schedstat_run_seconds_total": "Time Duration Processes of a Container have Run on the CPU (seconds)",
	"container_cpu_schedstat_run_periods_total": "Number of Times Processes of a Container have Run on the CPU (count)",
	"cycles_per_second": "Cycles per Second allocated to this Container (count/second)",
	"instruction_retirement_rate": "Rate at which Instructions were Executed (count/second)",
	"cycles_per_instruction": "Cycles Per Instruction",
	"container_processes": "Number of Processes Running Inside the Container (count)",
}

def plot_variations(args):
	global taskMetricsList
	global benchmarkProfilingData

	plot_dir = os.path.join("plots", args.kind, args.benchmark, "powerclass-"+args.powerclass[0], "correlation_with_"+args.correlation_target_metric, "correlation_criteria__"+args.correlation_criteria, args.host)
	if not os.path.exists(plot_dir):
		os.makedirs(plot_dir, exist_ok=True)

	y_data = []
	if "exectime" in args.correlation_target_metric:
		# get execution time array (y axis).
		for met_agg_data in benchmarkProfilingData['metrics_aggregate_data']:
			y_data.append(met_agg_data['perf_metrics']['exec_time_seconds'])
	if "cpuquota" in args.correlation_target_metric:
		# get cpu quota values (y axis). 
		for met_agg_data in benchmarkProfilingData['metrics_aggregate_data']:
			y_data.append(met_agg_data['run_config']['cpu_quota'])

	yaxis_title = ""
	if "exectime" in args.correlation_target_metric:
		yaxis_title = "Execution Time (seconds)"
	if "cpuquota" in args.correlation_target_metric:
		yaxis_title = "CPU Quota (cpu period = 100 milliseconds)"

	for task_metric in taskMetricsList:
		x_metrics_data = []
		for met_agg_data in benchmarkProfilingData['metrics_aggregate_data']:
			x_metrics_data.append(met_agg_data['task_metrics'][task_metric][args.correlation_criteria])

		# plot and save.
		traces = [go.Scatter(x=x_metrics_data, y=y_data, mode='lines+markers')]
		layout = go.Layout(title=dict(text="Benchmark: "+args.benchmark, font=dict(size=28, color='black')), xaxis=dict(title=plotTitlesForTaskMetrics[task_metric]), yaxis=dict(title=yaxis_title))
		fig = go.Figure(data=traces, layout=layout)
		image_name = ""
		if "exectime" in args.correlation_target_metric:
			image_name = task_metric+"_vs_exectime.png"
		if "cpuquota" in args.correlation_target_metric:
			image_name = task_metric+"_vs_cpuquota.png"
		# py.image.save_as(fig, os.path.join(plot_dir, task_metric+"_vs_exectime.png"))
		fig.write_image(os.path.join(plot_dir, image_name))

def plot_correlations(args):
	global taskMetricsList
	global benchmarkProfilingData
	global corrcoefs_exectime
	global corrcoefs_cpuquota

	plot_dir = os.path.join("plots", args.kind, args.benchmark, "powerclass-"+args.powerclass[0], "correlation_with_"+args.correlation_target_metric, "correlation_criteria__"+args.correlation_criteria, args.host)
	if not os.path.exists(plot_dir):
		os.makedirs(plot_dir, exist_ok=True)
	
	metric_correlation_values = []
	for task_metric in taskMetricsList:
		if "exectime" in args.correlation_target_metric:
			metric_correlation_values.append(corrcoefs_exectime[task_metric])
		if "cpuquota" in args.correlation_target_metric:
			metric_correlation_values.append(corrcoefs_cpuquota[task_metric])

	bar_colors = [
			'rgb(127,201,127)', 
			'rgb(190,174,212)', 
			'rgb(253,192,134)', 
			'rgb(166,206,227)', 
			'rgb(31,120,180)', 
			'rgb(178,223,138)', 
			'rgb(51,160,44)', 
			'rgb(251,154,153)', 
			'rgb(227,26,28)',
			'rgb(253,191,111)',
			'rgb(255,127,0)',
			'rgb(202,178,214)',
			'rgb(53,151,143)',
			'rgb(100,150,200)'
	]

	trace = go.Bar(x=taskMetricsList, y=metric_correlation_values, marker_color=bar_colors)

	yaxis_title = ""
	if "exectime" in args.correlation_target_metric:
		yaxis_title = "Correlation Coefficient w.r.t Execution Time"
	if "cpuquota" in args.correlation_target_metric:
		yaxis_title = "Correlation Coefficient w.r.t CPU Quota"

	layout = {
		'title': {
			'text': "Benchmark: "+args.benchmark,
			'font': {
				'size': 28,
				'color': 'black'
			}
		},
		'xaxis': {
			'title': ' ',
			'titlefont': {'size': 28, 'color': 'black'},
            'tickfont': {'size': 28, 'color': 'black'},
            'automargin': True,
            'tickangle': -45
		},
		'yaxis': {
            'title': yaxis_title,
            'titlefont': {'size': 27, 'color': 'black'},
            'tickfont': {'size': 30, 'color': 'black'},
            'automargin': True,
            'range':[-1,1]
        },
        'legend': {
            'x': 0.02,
            'y': 1.1,
            'font': {'size': 26, 'color': 'black'},
            'borderwidth': 1
        },
		'height': 1400,
		'width': 1000,
        'margin': go.layout.Margin(t=40, b=10, r=10)
	}

	fig = go.Figure(layout=layout)
	fig.add_trace(trace)
	image_name = ""
	if "exectime" in args.correlation_target_metric:
		image_name = "metric_correlation_wrt_execution-time.png"
	if "cpuquota" in args.correlation_target_metric:
		image_name = "metric_correlation_wrt_cpuquota.png"
	fig.write_image(os.path.join(plot_dir, image_name))


def main(args):
	load_nodes_info(args)
	if setup(args):
		persistBenchmarkProfilingData(args)
		correlate(args)
		persistCorrelationData(args)
		if args.plot:
			plot_variations(args)
			plot_correlations(args)

if __name__ == "__main__":
	args = setup_args()
	main(args)
