#!/bin/bash

kind=$1
benchmark=$2

#######################
###### CPU QUOTA ######
#######################

# powerclass = C
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

# powerclass = A
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

# powerclass = B
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric cpuquota --correlation-criteria 90PCTL --plot

########################
#### EXECUTION TIME ####
########################

# powerclass = C
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-001 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-002 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-003 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass C --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-004 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

# powerclass = A
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-005 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass A --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-006 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

# powerclass = B
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-007 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot

python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria average --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria median --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 95PCTL --plot
python3 correlate_factors.py --kind $kind --powerclass B --nodes-cpuarchinfo nodes.json --benchmark $benchmark --host stratos-008 --pcplog-freq-seconds 1 --correlation-target-metric exectime --correlation-criteria 90PCTL --plot
